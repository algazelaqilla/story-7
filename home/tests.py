from django.test import TestCase, Client
from django.urls import reverse, resolve
from home.views import index
# from .views import
# Create your tests here.

class HomeTestCase(TestCase):
    def test_url_home(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_TemplateUsed_counter(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_url_is_resolved_add(self):
        url = reverse('home:index')
        self.assertEqual(resolve(url).func, index)